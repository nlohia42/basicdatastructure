package com.ie.source;

import java.util.Scanner;

public class DeletionOfAnElementFromLinkedList {

	Node head;

	static class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
		}
	}

	/* Given a key, deletes the first occurrence of key in linked list */
	public void deleteNode(int key) {
		// Store head node
		Node temp = head, prev = null;

		// If head node itself holds the key to be deleted
		if (temp != null && temp.data == key) {
			head = temp.next;
			return;
		}

		// Search for the key to be deleted, keep track of the
		// previous node as we need to change temp.next
		while (temp != null && temp.data != key) {
			prev = temp;
			temp = temp.next;
		}

		// If key was not present in linked list
		if (temp == null)
			return;

		// Unlink the node from linked list
		prev.next = temp.next;
	}

	/* Inserts a new Node at front of the list. */
	public void push(int new_data) {
		Node new_node = new Node(new_data);
		new_node.next = head;
		head = new_node;
	}

	/*
	 * This function prints contents of linked list starting from the given node
	 */
	public void printList() {
		Node tnode = head;
		while (tnode != null) {
			System.out.print(tnode.data + " ");
			tnode = tnode.next;
		}
	}

	public static void main(String[] args) {
		DeletionOfAnElementFromLinkedList llist = new DeletionOfAnElementFromLinkedList();

		llist.push(7);
		llist.push(1);
		llist.push(3);
		llist.push(2);

		System.out.println("\nCreated Linked list is:");
		llist.printList();

		Scanner sc = new Scanner(System.in);
		System.out.println("please enter input");
		int aa = sc.nextInt();
		llist.deleteNode(aa); // Delete node at position 4

		System.out.println("\nLinked List after Deletion at position :" + aa);

		llist.printList();

		System.out.println("adding element to the LinkedList");

		System.out.println("enter element");
		int dd = sc.nextInt();
		llist.push(dd);

		llist.printList();
	}
}
