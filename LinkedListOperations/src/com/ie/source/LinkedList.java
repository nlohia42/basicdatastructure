package com.ie.source;
public class LinkedList {
	Node head;
	static class Node {
		int data;
		Node next;
		Node(int data) {
			this.data = data;
		}
	}

	public void printList() {
		Node node = head;
		while (node != null) {
			System.out.println(node.data + " ");
			node = node.next;
		}
	}

	public static void main(String[] args) {
		LinkedList list = new LinkedList();
		list.head = new Node(1);
		Node node1 = new Node(2);
		Node node2 = new Node(3);
		list.head.next = node1;
		node1.next = node2;
		list.printList();
	}

}
