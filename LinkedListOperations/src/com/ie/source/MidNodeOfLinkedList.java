package com.ie.source;

public class MidNodeOfLinkedList {

	Node head;
	static class Node {
		int data;
		Node next;
		Node(int data){
			this.data = data;
		}
	}
	public void push(int data) {
		Node node = new Node(data);
		node.next= head;
		head = node;
	}
	public void printMid() {
		Node slowPointer = head;
		Node fastPointer = head;
		if(head!=null) {
			while((fastPointer!=null) && (fastPointer.next!=null)) {
				fastPointer = fastPointer.next.next;
				slowPointer = slowPointer.next;
			}
			System.out.println("middle element ::"+slowPointer.data);
		}
	}
	public void printList() {
		Node node = head;
		while (node != null) {
			System.out.println("data ::"+node.data +" ");
			node = node.next;
		}
	}
	public static void main(String[] args) {
		MidNodeOfLinkedList list = new MidNodeOfLinkedList();
		for(int i=0;i<5;i++) {
			list.push(i);
			list.printList();
			list.printMid();
		}
	}
}
